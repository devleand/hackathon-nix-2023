<?php

namespace App\Enum;

enum Slot: string
{
    case EMPTY = '_';
    case BLOCKED = 'x';

    case FIRST_PLAYER = 'f';
    case SECOND_PLAYER = 's';

    public static function fromString(string $value): self
    {
        return self::from(strtolower($value));
    }
}
