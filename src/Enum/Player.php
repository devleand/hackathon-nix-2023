<?php

namespace App\Enum;

enum Player: string
{
    case FIRST = 'f';
    case SECOND = 's';

    public static function fromString(string $value): self
    {
        return self::from(strtolower($value));
    }
}
