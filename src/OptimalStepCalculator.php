<?php

namespace App;

use App\Dto\BoardDto;
use App\Dto\SlotDto;
use App\Dto\StepDto;
use App\Enum\Player;
use App\Enum\Slot;

class OptimalStepCalculator
{
    private const OPPOSITE_DIRECTION_METHODS = [
        ['moveLeft', 'moveRight'],
        ['moveTop', 'moveBottom'],
        ['moveLeftTop', 'moveRightBottom'],
        ['moveLeftBottom', 'moveRightTop']
    ];

    public function calculate(Player $player, BoardDto $boardDto): StepDto
    {
        $board = $boardDto->getBoard();
        $stepCandidates = $this->getStepCandidates($board);
        $optimalStep = StepDto::createFromSlot($stepCandidates[0], 0);

        foreach ($stepCandidates as $stepCandidate) {
            foreach (self::OPPOSITE_DIRECTION_METHODS as $oppositeDirectionMethods) {
                $maxFilledSlotsCount = 0;
                foreach ($oppositeDirectionMethods as $oppositeDirectionMethod) {
                    $currentSlot = $stepCandidate;
                    while ($currentSlot = $currentSlot->{$oppositeDirectionMethod}()) {
                        if ($player->value === Slot::fromString($board[$currentSlot->getRow()][$currentSlot->getColumn()])->value) {
                            $maxFilledSlotsCount++;
                        } else {
                            break;
                        }
                    }
                }

                if ($maxFilledSlotsCount > $optimalStep->getFilledSlotsCount()) {
                    $optimalStep = StepDto::createFromSlot($stepCandidate, $maxFilledSlotsCount);
                }
            }
        }

        return $optimalStep;
    }

    /**
     * @param array $board
     *
     * @return array<SlotDto>
     */
    private function getStepCandidates(array $board): array
    {
        $candidates = [];
        for ($i = 0; $i < BoardDto::COLUMNS_COUNT; $i++) {
            for ($j = BoardDto::ROWS_COUNT - 1; $j >= 0; $j--) {
                if (Slot::EMPTY === Slot::fromString($board[$j][$i])) {
                    $candidates[] = new SlotDto($j, $i);
                    break;
                }
            }
        }

        return $candidates;
    }
}
