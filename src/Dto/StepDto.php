<?php

namespace App\Dto;

class StepDto
{
    private int $column;

    private int $filledSlotsCount;

    public function __construct(int $column, int $filledSlotsCount)
    {
        $this->column = $column;
        $this->filledSlotsCount = $filledSlotsCount;
    }

    public static function createFromSlot(SlotDto $slotDto, int $filledSlotsCount): self
    {
        return new self($slotDto->getColumn(), $filledSlotsCount);
    }

    public function getColumn(): int
    {
        return $this->column;
    }

    public function getFilledSlotsCount(): int
    {
        return $this->filledSlotsCount;
    }
}
