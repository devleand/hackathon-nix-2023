<?php

namespace App\Dto;

class BoardDto
{
    public const COLUMNS_COUNT = 9;
    public const ROWS_COUNT = 6;

    private array $board;

    public function __construct(array $board)
    {
        $this->board = $board;
    }

    public function getBoard(): array
    {
        return $this->board;
    }
}
