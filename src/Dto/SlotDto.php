<?php

namespace App\Dto;

class SlotDto
{
    private int $row;
    private int $column;

    private bool $isFirstColumn;
    private bool $isLastColumn;
    private bool $isFirstRow;
    private bool $isLastRow;

    public function __construct(int $row, int $column)
    {
        $this->row = $row;
        $this->column = $column;

        $this->isFirstColumn = 0 === $column;
        $this->isLastColumn  = BoardDto::COLUMNS_COUNT - 1 === $column;
        $this->isFirstRow    = 0 === $row;
        $this->isLastRow     = BoardDto::ROWS_COUNT - 1 === $row;
    }

    public function getColumn(): int
    {
        return $this->column;
    }

    public function getRow(): int
    {
        return $this->row;
    }

    public function moveLeft(): ?self
    {
        return $this->isFirstColumn ? null : new SlotDto($this->getRow(), $this->getColumn() - 1);
    }

    public function moveLeftTop(): ?self
    {
        return $this->isFirstColumn || $this->isFirstRow
            ? null
            : new SlotDto($this->getRow() - 1, $this->getColumn() - 1);
    }

    public function moveTop(): ?self
    {
        return $this->isFirstRow ? null : new SlotDto($this->getRow() - 1, $this->getColumn());
    }

    public function moveRightTop(): ?self
    {
        return $this->isLastColumn || $this->isFirstRow
            ? null
            : new SlotDto($this->getRow() - 1, $this->getColumn() + 1);
    }

    public function moveRight(): ?self
    {
        return $this->isLastColumn ? null : new SlotDto($this->getRow(), $this->getColumn() + 1);
    }

    public function moveRightBottom(): ?self
    {
        return $this->isLastColumn || $this->isLastRow
            ? null
            : new SlotDto($this->getRow() + 1, $this->getColumn() + 1);
    }

    public function moveBottom(): ?self
    {
        return $this->isLastRow ? null : new SlotDto($this->getRow() + 1, $this->getColumn());
    }

    public function moveLeftBottom(): ?self
    {
        return $this->isFirstColumn || $this->isLastRow
            ? null
            : new SlotDto($this->getRow() + 1, $this->getColumn() - 1);
    }
}
