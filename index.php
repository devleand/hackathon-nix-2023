<?php

use App\Dto\BoardDto;
use App\Enum\Player;
use App\OptimalStepCalculator;

require_once __DIR__ . '/vendor/autoload.php';

try {
    $requestBody = json_decode(file_get_contents('php://input'),  true);
    if (
        !$requestBody
        || !array_key_exists('gameId', $requestBody)
        || !array_key_exists('board', $requestBody)
        || !array_key_exists('player', $requestBody)
    ) {
        throw new RuntimeException('Invalid request body.');
    }

    $calculator = new OptimalStepCalculator();
    $player = Player::fromString($requestBody['player']);
    $boardDto = new BoardDto($requestBody['board']);
} catch (Throwable $exception) {
    http_response_code(400);
    exit('Bad request');
}

exit(json_encode([
    'column' => $calculator->calculate($player, $boardDto)->getColumn(),
]));
